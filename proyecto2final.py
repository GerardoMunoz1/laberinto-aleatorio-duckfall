import random
import os
import time

# La variable mapa es una lista de listas.
mapa = []

# Se le agregan 21 listas a la lista general.
# En cada una se le agregan 21 elementos. (21x21)
for i in range(0, 21):
    mapa.append([])
    for j in range(0, 21):
        mapa[i].append(0)

# El laberinto empezará a construirse a partir de celdaInicial.
celdaInicial = [19, 19]

# La meta estará justo al lado de donde se empezó a construir el mapa.
# Es decir, 19x20. Luego, definimos el grafico 19x20 a una banderita.
mapa[19][20] = 4

# Aquí se almacenarán todas las celdas que se hayan recorrido.
celdasRecorridas = []

print ("")
print ("          ==BIENVENIDO A DUCKFALL==")

# La seed del mapa.
seedmapa = random.randint(10000, 99999)
print("")
print("La seed del mapa es:", seedmapa)
random.seed(seedmapa)

# Esta función toma una celda y chequea las 4 celdas que la rodean
# (Celdas vecinas), entonces elige una al azar. Luego define la
# celda inicial, la celda vecina elegida y la celda que esta entre
# medio como "caminos". Luego se repite el algoritmo con la celda
# vecina elegida.
def abrirCamino(celda, volver):

    # Si estábamos en una celda siguiente y no tenía vecinos y
    # luego nos devolvimos a una anterior, volver sera True. Por
    # el contrario, si volver es false, se añade la celda actual
    # a celdasRecorridas. Si es True, significa que ya estaba
    # añadida desde antes.
    if(volver == False):
        celdasRecorridas.append(celda)

    # Coordenadas de las celdas vecinas.
    vecinos = [[celda[0] - 2, celda[1]],
               [celda[0] + 2, celda[1]],
               [celda[0], celda[1] - 2],
               [celda[0], celda[1] + 2]]

    # Aquí se almacenarán todos los vecinos que estén dentro
    # del mapa y que no hayan sido visitados antes.
    vecinosVisitables = []

    # Chequea los 4 vecinos. Si sus coordenadas están entre 0 y 20,
    # significa que podemos acceder a ellos (están dentro del mapa).
    for i in range(0, 4):
        if((vecinos[i][0] >= 0 and vecinos[i][0] < 20) and (vecinos[i][1] >= 0
           and vecinos[i][1] < 20)):
            if(mapa[vecinos[i][0]][vecinos[i][1]] == 0):
                vecinosVisitables.append(vecinos[i])

    # Si hay más de un elemento en vecinosVisitables.
    if(len(vecinosVisitables) > 0):
        a = random.randint(0, len(vecinosVisitables) - 1)

        # celdaActual = celda original
        # celdaNueva = vecino elegido
        # celdaEntreMedio = celda que está entre la original y
        # el vecino elegido.
        celdaActual = celda
        celdaNueva = vecinosVisitables[a]
        celdaEntreMedio = [int((celda[0] + celdaNueva[0]) / 2),
                           int((celda[1] + celdaNueva[1]) / 2)]

        # Se define a las 3 como 'camino'.
        mapa[celda[0]][celda[1]] = 1
        mapa[celdaNueva[0]][celdaNueva[1]] = 1
        mapa[celdaEntreMedio[0]][celdaEntreMedio[1]] = 1

        # Ahora hacemos lo mismo con la celdaNueva.
        seguirAdelante(celdaNueva)
    else:

        # Si no tiene vecinos, volvemos a la celda anterior.
        volverAtras()

# Se abre camino con la celda siguiente.
def seguirAdelante(celdaSiguiente):
    abrirCamino (celdaSiguiente, False)

# Volvemos a la celda anterior y abrimos camino con ella.
def volverAtras():

    # La función 'pop' eliminará el último elemento de la lista, se
    # elimina la última celda recorrida y se devuelve a la anterior.
    celdasRecorridas.pop()
    if(len(celdasRecorridas) == 0):
        imprimir_mapa(mapa)
    else:
        abrirCamino(celdasRecorridas[len(celdasRecorridas) - 1], True)

jugador = [1, 0]

# Función para crear el menú.
def menuprincipal ():
    opcion = 0
    print ("         == MENU ==")
    print ("Presionar [0] para volver a jugar.")
    print ("Presionar [1] para ingresar una semilla determinada.")
    print ("Presionar [2] para salir.")
    opcion = input("OPCION: ")
    comprobarmenu(opcion)

# Función para reinicar el juego.
def comprobarmenu (opcion):
    if (opcion == "0"):
        print ("volvamo a jugar")
    elif (opcion == "1"):
        semillaingresada = input("Ingrese semilla: ")
        if (semillaingresada == seedmapa):
            random.seed(seedmapa)
        else:
            print ("No tenemos registro de esa semilla :(")

    elif(opcion == "2"):
        print ("Gracias por jugar.")
        Gano = 1
    else:
        print ("nothing")
    return (Gano)

# Función para imprimir el mapa.
def imprimir_mapa (mapa1):
    print("                                             ")
    for i in range (0,21):
        linea = ""
        for x in range (0,21):
            casilla = mapa1[i][x]
            elementos = ["⬜","⬛", "🦆","🏠","🏁","  "]
            if (jugador[1] == x and jugador[0] == i):
                linea = linea + elementos [2]
            else:
                linea = linea + elementos [int (casilla)]
        print (linea)

# Creación del laberinto a partir de la celda inicial.
abrirCamino(celdaInicial, False)

# Función para jugar.
Gano = 0
while (Gano != 1):
    movimiento = input("MOVERSE: ")
    if (movimiento == "W" or movimiento == "w"):
        if (mapa[jugador[0] - 1][jugador[1]] != 0):
            jugador[0] = jugador[0] - 1

    elif (movimiento == "S" or movimiento == "s"):
        if (mapa[jugador[0] + 1][jugador[1]] != 0):
            jugador[0] = jugador[0] + 1

    elif (movimiento == "A" or movimiento == "a"):
        if (mapa[jugador[0]][jugador[1] - 1] != 0):
            jugador[1] = jugador[1] - 1

    elif (movimiento == "D" or movimiento == "d"):
        if (mapa[jugador[0]][jugador[1] + 1] != 0):
            jugador[1] = jugador[1] + 1

    os.system('clear')
    print ("SEED:", seedmapa)
    imprimir_mapa(mapa)

    # Victoria.
    if (mapa[jugador[0]][jugador[1]] == 4):
        print ("¡Ganaste!")
        time.sleep (0.5)
        os.system('clear')
        menuprincipal()
        if (Gano == "1"):
            Gano = 1
        else:
            Gano = 0
    else:
        print ("")
