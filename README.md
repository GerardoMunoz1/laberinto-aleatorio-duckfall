# Laberinto Aleatorio DuckFall
Segundo proyecto de pogramación: Creación de una IA que sea capaz de jugar 
laberintos aleatorios.

-> Plantemiento de la solución:

-Para la realización de este proyecto se pensó, en primer lugar, como se podría
hacer el laberinto aleatorio, ya que era indispensable que se generaran
laberintos aleatoriamente. Para ello se pensó en las  condicionales que se
necesitaban para que al iniciar el programa los laberintos fuesen completamente
distinto entre sí y que se generara siempre un camino hacia la salida. Luego de
terminado este paso, se hicieron las condicionales para que el laberinto fuese
jugable.

-> Descripción del juego: 

-El laberinto es una especie de "recinto" que tiene, generalmente, la entrada y
la salida en distintos lugares; el cual, a su vez, está conformado por caminos 
muy parecidos que se entrecruzan entre sí y que se disponen de tal manera que 
resulta difícil y confuso orientarse para alcanzar la salida.

-> Objetivo del juego:

-La finalidad del juego presentado no es nada más y nada menos que llegar al otro
extremo del laberinto, el cual será la salida; más detalladamente sería el 
correcto sorteo de los caminos que se presentan para, de este modo, alcanzar
la meta.

-> Fallos que posee el programa:

-Al momento de preguntar con cual tecla desea moverse el jugador, el programa 
no reconoce las teclas de dirección (Flechas) ni ningún número, solo acepta las
teclas de movimientos indicadas en el programa.

-> Aspectos mejorables:

-Implementación de una generación completamente aleatoria de salidas del
laberinto al momento de ejecutar el código otorgando, de este modo, una mayor
diversión y variabilidad en cuanto a los posibles caminos del laberinto en sí.
-Creación de una IA a la que se pudiesen enseñar una gran diversidad de caminos
mientras se jugaba, de este modo, al ejecutarse esta IA, el laberinto se podría
completar solo, llevando al jugador por el camino más corto, más largo, también
pudiendo dar vuelta en círculos, etc.  
-No tener que presionar "enter" al momento de realizar cada movimiento.
-Al momento de ingresar 3 veces la misma letra
(Por ejemplo: "AAA" y luego enter), que el "personaje" se mueva tres casillas 
en la dirección ingresada. De este modo se haría mucho más rápida la jugabilidad
del mismo.

-> Aspectos incluidos de la PEP 8:

-Solo se utilizaron espacios, para no crear una inconsistencia en el programa 
con la utilización de tabulaciones (4 espacios).
-Se respetaron los 4 espacios que deben existir entre una función y los elementos
de la misma.
-Los comentarios se escribieron sobre cada línea de código con el fin de lograr
explicarlo de una manera más adecuada.
-Los espacios necesarios entre una función y otra se respetan (2).
-No hay lineas de código que superen los 79 caracteres.
-No hay comentarios que superen las 72 lineas de caracteres.

